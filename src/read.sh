#!/bin/bash

file=/home/iiot/tsn/test.log

OUTPUT="DataSequenceNumber MessageLength PublisherTimestamp SubscriberTimestamp \n\n"
while read line;do
   	keyword="$( echo $line | awk '{print $(NF-1), $NF}')"

	if [ "$( echo $keyword | awk '{print $(NF-1)}')" == "dataSetMessageSequenceNr:" ]; then
		dataSetMessageSequenceNr="$(echo $keyword | awk '{print $(NF)}')"
	elif [ "$( echo $keyword | awk '{print $(NF-1)}')" == "length:" ]; then
		length="$(echo $keyword | awk '{print $(NF)}')"
	elif [ "$( echo $keyword | awk '{print $(NF-1)}')" == "Publisher:timestamp:" ]; then
		PublisherTimestamp="$(echo $keyword | awk '{print $(NF)}')"
	elif [ "$( echo $keyword | awk '{print $(NF-1)}')" == "Subscriber:timestamp:" ]; then
		SubscriberTimestamp="$(echo $keyword | awk '{print $(NF)}')"
	fi

	if [ $dataSetMessageSequenceNr ] && [ $length ] && [ $PublisherTimestamp ] && [ $SubscriberTimestamp ]; then
	OUTPUT="${dataSetMessageSequenceNr} ${length} ${PublisherTimestamp} ${SubscriberTimestamp}"
	echo $OUTPUT | column -t
	fi

	done < "$file"
