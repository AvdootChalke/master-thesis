#!/bin/bash

#create a variable to represent the filename
COUNTER_FILE="data.txt"
THROUGHPUT_FILE="throughput.txt"
SYNC_FILE="sync.txt"

usage() { echo "Usage: $0 [-t  elapsed filelog] [-h	help] [-i	interface for data throughput] [-s	syncfile] [-p	generate the data file from test.log] " 1>&2; exit 1; }

gnuplotgraph(){
echo "Plot graph"
}

elapsedtime() {
rm -rf elapse.log
file="$1"
VAR1=$(cat "$file" | awk '{print $(NF)}'  | grep -o '[A-Z]\+')
VAR2=$(cat "$file" | awk '{print $(NF)}'  | grep -o '[0-9]\+')
VAR3=$(cat "$file" | awk '{print $(1)}'  | grep -o '[0-9]\+')
paste <(printf %s "$VAR3") <(printf %s "$VAR2") | tee -a elapse.log
}

datathroughput() {
	rm -rf "$THROUGHPUT_FILE"
	n=1
	while [ $n -le 500 ];do
		received_bytes=$(cat /sys/class/net/$interface/statistics/rx_bytes)
		transmit_bytes=$(cat /sys/class/net/$interface/statistics/tx_bytes)
		sleep 1
		received_bytes1=$(cat /sys/class/net/$interface/statistics/rx_bytes)
		transmit_bytes1=$(cat /sys/class/net/$interface/statistics/tx_bytes)
		throughput_transmit=$((transmit_bytes1 - transmit_bytes))
		throughput_receive=$((received_bytes1 - received_bytes))
		echo "$throughput_transmit"$'\r' >> "$THROUGHPUT_FILE"
		n=$(( n+1 ))
	done
}

timesync(){
	rm -rf "$SYNC_FILE"
	while read line;do
		echo "$line" | grep '[0-9]$' | awk '{print $NF}' >>"$SYNC_FILE"
	done < "$sync"

}

getdatafile() {
rm -rf ac.log
file="$1"
VAR1=$(cat "$file" | grep "dataSetMessageSequenceNr" | cut -d' ' -f 5 | cut -d':' -f1)
VAR2=$(cat "$file" | grep "Message length:" | cut -d' ' -f 6 | cut -d':' -f1)
VAR3=$(cat "$file" | grep "Publisher:timestamp:" | cut -d' ' -f 5 | cut -d':' -f1)
VAR4=$(cat "$file" | grep "Subscriber:timestamp:" | cut -d' ' -f 5 | cut -d':' -f1)
VAR5=$(cat "$file" | grep "Delivery time:" | cut -d' ' -f 6 | cut -d':' -f1)
VAR6="SNr"
VAR7="MSize"
VAR8="PublisherTimestamp"
VAR9="SubscriberTimestamp"
VAR10="ELAPSEDTIME"

paste <(printf %s "$VAR6") <(printf %s "$VAR7") <(printf %s "$VAR8") <(printf %s "$VAR9") <(printf %s "$VAR10") | tee -a ac.log
paste <(printf %s "$VAR1") <(printf %s "$VAR2") <(printf %s "$VAR3") <(printf %s "$VAR4") <(printf %s "$VAR5")  | tee -a ac.log
}

while getopts ":t:hi:s:p:" o; do
	case "${o}" in
		t)
			filename="$OPTARG"
			if [ -f "$filename" ]; then
				elapsedtime "$filename"
			else
				echo "provide the elapsed data log file!"
				exit 1
			fi
			;;
		h)
			usage
			;;
		i)
			interface="$OPTARG"
			datathroughput "$interface"
			echo $interface
			;;
		s)
			sync="$OPTARG"
			if [ -f "$sync" ]; then
                                timesync "$sync"
                        else
                                echo "provide the sync log file!"
                                exit 1
                        fi
			;;
		p) 
			getdata="$OPTARG"
			if [ -f "$getdata" ]; then
                                getdatafile "$getdata"
                        else
                                echo "provide the data log file named datafile!"
                                exit 1			
                        fi
                        ;;

		*)
			usage
			exit 1
			;;
	esac
done
shift $((OPTIND-1))

#if [ -z "${t}" ]; then
#    usage
#fi
