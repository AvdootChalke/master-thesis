#!/bin/bash

#Works with network interface card intel i210 and linux kernel version >= 5.0

iface=$1
basetime=$2
gatesched=$3

#Check input arguments
if [ -z $iface ]; then 
        echo "provide network interface"
        exit 1
elif [ -z $basetime ]; then
        echo "provide time in minutes for basetime"
        exit 1
elif [ -z $gatesched ]; then
	echo "Provide the gate sched file"
	exit 1			        
fi

#Reading the sched file
function schedfile 
{
	while read -r entries; do
	echo "sched-entry "$entries
	done < $gatesched
}

#Enable Taprio,Mqprio and ETF
sudo modprobe -i sch_taprio 
sudo modprobe -i sch_mqprio
sudo modprobe -i sch_etf

#Reading the map config file 


#Setup taprio with a base-time starting in $basetime from now rounded down. We must add the 37s UTC-TAI offset to the timestamp we get with 'date'.
i=$((`date +%s%N` + 37000000000 + ( $basetime * 60 * 1000000000)))
BASE_TIME=$(($i - ($i % 1000000000)))
echo $BASE_TIME

sched_entry=$(schedfile)

sudo tc qdisc replace dev $iface parent root handle 100 taprio \
       num_tc 2 \
       map 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 \
       queues 1@0 3@1 \
       base-time $BASE_TIME \
       $sched_entry \
       clockid CLOCK_TAI

if [ $? -eq 0 ];  then
       echo "Taprio set!"
       sudo tc qdisc show dev $iface
else 
        echo "Taprio not set!"
        exit 1
fi

#sudo tc qdisc replace dev $iface parent 100:1 etf \
#        offload delta 200000 clockid CLOCK_TAI

